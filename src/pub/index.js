const app = require('../app');
const config = require('../config');


app.listen(config.http_port, () => {
    console.log(`Listening on port ${config.http_port} ...`);
});

