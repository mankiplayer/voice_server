const Config = {
    http_port: 3000,
    websocket_port: 3001,
    google_project_id: 'kakao-speech-sample',
    storage_dir: `${process.cwd()}/storage`
};


module.exports = Config;

