const fs = require('fs');
const config = require('../config');
const PCMStream = require('./pcm-stream');


const PCMArchiver = new class {
    constructor() {
        this._streamOptions = {
            flags: 'w',
            defaultEncoding: 'binary'
        };
    }

    createWriteStream() {
        return new PCMStream(this._streamOptions);
    }

    readFile(fileId) {
        return new Promise((resolve, reject) => {
            const filepath = `${config.storage_dir}/${fileId}.wav`;
            fs.readFile(filepath, (err, data) => {
                if (err) reject(err);
                else resolve(data);
            });
        });
    }
}


module.exports = PCMArchiver;

