const SpeechClient = require('@google-cloud/speech');
const config = require('../config');


class RecognizeRequest {
    constructor(inputStream, recognizeStream) {
        this._recognizeStream = recognizeStream;
        this._recognizeStream.on('data', this._onData.bind(this));
        this._recognizeStream.on('error', this._onError.bind(this));

        if (inputStream) inputStream.pipe(this._recognizeStream);

        this._onEvent = null;
        this._finished = false;
    }

    set onEvent(callback) {
        if (typeof callback !== 'function') throw new Error('Callback must be a function.');
        this._onEvent = callback;
    }

    feedAudio(data) {
        if (!this._finished) this._recognizeStream.write(data);
    }


    ////////////////////////////////////////////////////////////////////////////
    // private methods

    _onData(data) {
        if (data.error) return this._onError(data.error.message || 'Error');
        console.log(data);

        let type = null;
        let args = [];
        switch (data.endpointerType) {
            case 'START_OF_SPEECH':
                type = 'bos';
                break;
            case 'END_OF_UTTERANCE':
                type = 'eos';
                break;
            case 'END_OF_AUDIO':
                this._finished = true;
                this._recognizeStream.end();
                break;
            default:
                type = 'transcription';
                args.push(data.results);
                break;
        }

        if (this._onEvent && type) this._onEvent({
            type: type, args: args
        });
    }

    _onError(err) {
        console.error(err);
        if (this._onError) this._onEvent({
            type: 'error', args: err.message
        });
    }
}


const Recognizer = new class {
    constructor() {
        this._speech = SpeechClient({projectId: 'kakao-speech-sample'});
        this._config = {
            config: {
                encoding: 'LINEAR16',
                sampleRate: 16000,
                languageCode: 'ko-KR',
                maxAlternatives: 1
            },
            singleUtterance: true,
            interimResults: true
        };
    }

    createRequest(inputStream = null) {
        const recognizeStream = this._speech.createRecognizeStream(this._config);
        return new RecognizeRequest(inputStream, recognizeStream);
    }
}


module.exports = Recognizer;

