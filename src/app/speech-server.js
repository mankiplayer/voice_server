const WebSocket = require('ws');
const Recognizer = require('./recognizer');
const PCMArchiver = require('./pcm-archiver');


class SpeechServer {

    ////////////////////////////////////////////////////////////////////////////
    // Public methods

    constructor() {
        this._ws = null;
        this._pcmStream = null;
        this._recognizeRequest = null;
    }

    run(server, port) {
        return new Promise(resolve => {
            const wss = new WebSocket.Server({server, port});
            wss.on('connection', (ws) => {
                console.log('connected ...');
                this._ws = ws;
                this._ws.on('message', this._onMessageFromClient.bind(this));
            });

            resolve();  // return connected
        });
    }


    ////////////////////////////////////////////////////////////////////////////
    // Private methods

    _sendEvent(type, args) {
        if (!args) args = [];
        args = (Array.isArray(args) ? args : [args]);
        try {
            const data = JSON.stringify({type, args});

            console.log(`send event... ${data}`);
            this._ws.send(data);
        } catch (err) {
            console.error(err);
        }
    }

    _sendError(message) {
        this._sendEvent('error', [message || 'Error occurred.']);
    }

    _onMessageFromClient(data, flags) {
        if (flags.binary) this._handleBinaryData(data);
        else this._handleStringData(data);
    }

    _onEventFromRecognizer(event) {
        this._sendEvent(event.type, event.args);
    }

    _handleBinaryData(data) {   // Handling PCM stream
        // Save PCM
        if (this._pcmStream) this._pcmStream.write(data);
        if (this._recognizeRequest) this._recognizeRequest.feedAudio(data);
    }

    _handleStringData(data) {   // Handling command
        try {
            const command = JSON.parse(data);
            console.log(command);

            switch (command.type) {
                case 'start':
                    if (this._busy || this._pcmStream) return;

                    // Open PCM stream to write
                    this._pcmStream = PCMArchiver.createWriteStream();
                    // Establish a session with Google speech server
                    this._recognizeRequest = Recognizer.createRequest();
                    this._recognizeRequest.onEvent = this._onEventFromRecognizer.bind(this);
                    break;
                case 'stop':
                    const pcmStream = this._pcmStream;

                    // Save speech file then send DONE event
                    this._pcmStream = null;
                    pcmStream.end().then(fileId => {
                        this._sendEvent('done', fileId);
                    });
                    break;
                case 'reqfile':
                    const fileId = command.args[0];
                    PCMArchiver.readFile(fileId).then(data => {
                        // socket으로 파일 반환
                        this._ws.send(data);
                    }).catch(err => console.error);
                    break;
                /*
                case 'translate':
                    const text = command.args[0];
                    const langCode = command.args[1];
                    break;
                */
                default:
                    break;
            }
        } catch (err) {
            console.error(err);
        }
    }
}


module.exports = SpeechServer;

