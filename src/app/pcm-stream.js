const fs = require('fs');
const tmp = require('tmp');
const crypto = require('crypto');
const config = require('../config');


class PCMStream {

    ////////////////////////////////////////////////////////////////////////////
    // Public methods

    constructor() {
        const tmppath = tmp.tmpNameSync({template: '/tmp/record_XXXXXX.pcm'});

        this._tmppath = tmppath;
        this._tmpfile = fs.createWriteStream(tmppath, {flags: 'w', defaultEncoding: 'binary'});
        this._isClosed = false;
    }

    write(data) {
        if (this._isClosed) return;
        this._tmpfile.write(data);
    }

    end() {
        if (this._isClosed) return;

        return new Promise(resolve => {
            this._tmpfile.end(null, null, () => {
                this._isClosed = true;

                PCMStream._getFileSize(this._tmppath)
                .then(filesize => {
                    const wavHeader = PCMStream._createWAVHeader(filesize);
                    const file = fs.createReadStream(this._tmppath);
                    const hash = crypto.createHash('md5');

                    return new Promise(resolve => {
                        hash.update(wavHeader);
                        file.on('data', (chunk) => {
                            hash.update(chunk);
                        });
                        file.on('end', () => {
                            resolve([hash.digest('hex'), wavHeader]);
                        });
                    });
                })
                .then(([md5sum, wavHeader]) => {
                    const from = fs.createReadStream(this._tmppath);
                    const to = fs.createWriteStream(`${config.storage_dir}/${md5sum}.wav`);

                    to.write(wavHeader);
                    from.pipe(to);
                    to.on('finish', () => { resolve(md5sum); });
                });
            });
        });
    }


    ////////////////////////////////////////////////////////////////////////////
    // Private methods

    static _getFileSize(filepath) {
        return new Promise((resolve, reject) => {
            fs.stat(filepath, (err, stats) => {
                if (err) reject(err);
                else resolve(stats.size);
            });
        });
    }

    static _createWAVHeader(filesize) {
        const SAMPLE_RATE = 16000;  // Number of output samples per second (Hertz)
        const BYTES_PER_SAMPLE = 2; // 16-Bit(2-Byte) per sample
        const SIZE_OF_HEADER = 44;
        const SIZE_OF_DATA = filesize;

        let header = Buffer.alloc(SIZE_OF_HEADER);
        header.write('RIFF', 0);
        header.writeUInt32LE(36 + SIZE_OF_DATA, 4);
        header.write('WAVE', 8);
        header.write('fmt ', 12);
        header.writeUInt32LE(16, 16);
        header.writeUInt16LE(1, 20);
        header.writeUInt16LE(1, 22);
        header.writeUInt32LE(SAMPLE_RATE, 24);
        header.writeUInt32LE(SAMPLE_RATE * BYTES_PER_SAMPLE, 28);
        header.writeUInt16LE(BYTES_PER_SAMPLE, 32);
        header.writeUInt16LE(BYTES_PER_SAMPLE * 8, 34);
        header.write('data', 36);

        return header;
    }
}


module.exports = PCMStream;

