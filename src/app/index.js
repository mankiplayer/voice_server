const express = require('express');
const logger = require('morgan');
const config = require('../config');

const app = express();


// Setup middlewares and basic routing
app.use(logger('dev'));
app.use('/hello', (req, res) => {
    res.send('Hello!');
});


const server = require('http').createServer(app);
const SpeechServer = require('./speech-server');

new SpeechServer().run(server, config.websocket_port)
.then(() => { console.log('SpeechServer started.'); });


module.exports = app;

